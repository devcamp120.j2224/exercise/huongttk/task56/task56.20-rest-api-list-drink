package com.task56s20.drinkapi.controller;

import java.util.ArrayList;
import java.util.Date;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.task56s20.drinkapi.models.CDrink;

@RestController
@CrossOrigin
public class CDrinkController {
    @GetMapping("/drinks")
    public ArrayList<CDrink> getDrink() {

        ArrayList<CDrink> myDrinkList = new ArrayList<CDrink>();
        CDrink tratac = new CDrink(101, "TRATAc", "Trà Tắc", 15000, new Date(), new Date());
        CDrink coca = new CDrink(102, "COCA", "Cocacola", 10000, new Date(), new Date());
        CDrink pepsi = new CDrink(103, "PEPSI", "Pepsi", 12000, new Date(), new Date());
       
        myDrinkList.add(tratac);
        myDrinkList.add(coca);
        myDrinkList.add(pepsi);

        return myDrinkList;
    }
}
