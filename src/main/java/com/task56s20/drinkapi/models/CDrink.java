package com.task56s20.drinkapi.models;

import java.util.Date;

public class CDrink {
    private int id = 0;
    private String maNuocUong = "";
    private String tenNuocUong = "";
    private int donGia = 0;
    private Date ngayTao = new Date();
    private Date ngayCapNhat = new Date();
    
    public CDrink(int id, String maNuocUong, String tenNuocUong, int donGia, Date ngayTao, Date ngayCapNhat) {
        this.id = id;
        this.maNuocUong = maNuocUong;
        this.tenNuocUong = tenNuocUong;
        this.donGia = donGia;
        this.ngayTao = ngayTao;
        this.ngayCapNhat = ngayCapNhat;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMaNuocUong() {
        return maNuocUong;
    }

    public void setMaNuocUong(String maNuocUong) {
        this.maNuocUong = maNuocUong;
    }

    public String getTenNuocUong() {
        return tenNuocUong;
    }

    public void setTenNuocUong(String tenNuocUong) {
        this.tenNuocUong = tenNuocUong;
    }

    public int getDonGia() {
        return donGia;
    }

    public void setDonGia(int donGia) {
        this.donGia = donGia;
    }

    public Date getNgayTao() {
        return ngayTao;
    }

    public void setNgayTao(Date ngayTao) {
        this.ngayTao = ngayTao;
    }

    public Date getNgayCapNhat() {
        return ngayCapNhat;
    }

    public void setNgayCapNhat(Date ngayCapNhat) {
        this.ngayCapNhat = ngayCapNhat;
    }

    @Override
    public String toString() {
        return "CDrink [donGia=" + donGia + ", id=" + id + ", maNuocUong=" + maNuocUong + ", ngayCapNhat=" + ngayCapNhat
                + ", ngayTao=" + ngayTao + ", tenNuocUong=" + tenNuocUong + "]";
    }
}
